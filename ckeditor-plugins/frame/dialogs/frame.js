CKEDITOR.dialog.add('frame', function (editor) {
  return {
    title: 'Settings of the frame',
    minWidth: 200,
    minHeight: 100,
    contents: [
      {
        id: 'frame-div',
        label: 'Style of the border',
        elements: [
          {
            type: 'text',
            id: 'color',
            label: 'Border color (HTML format) of the frame :',
            validate: function () {
              if (!this.getValue()) {
                alert('Border color cannot be empty.');
                return false;
              }
              if (this.getValue().substring(0, 1) != '#') {
                alert('Border color must be a valid HTML color (must starts with a "#").');
                return false;
              }
              if (this.getValue().length !== 4 && this.getValue().length !== 7) {
                alert('Border color must be a valid HTML color (example: #fff or #fffff)');
                return false;
              }
            }
          },
          {
            type: 'text',
            id: 'thickness',
            label: 'Border thickness of the frame (in px):',
            validate: CKEDITOR.dialog.validate.notEmpty('This field cannot be empty')
          },
          {
            type: 'select',
            id: 'type',
            label: 'Select the frame border style (solid, dashed, dotted) :',
            items: [['solid'],['dotted'],['dashed']],
            default: 'solid'
          },
          {
            type: 'text',
            id: 'padding',
            label: 'Padding (expressed in px) :',
            validate: function () {
              if (!this.getValue()) {
                alert('Padding cannot be empty.');
                return false;
              }
              if (parseInt(this.getValue()) === 0) {
                alert('Padding must be upper than 0.');
                return false;
              }
            }
          },
          {
            type: 'text',
            id: 'backcolor',
            label: 'Background color (HTML format) of the frame :',
            validate: function () {
              if (!this.getValue()) {
                alert('Background color cannot be empty.');
                return false;
              }
              if (this.getValue().substring(0, 1) != '#') {
                alert('Background color must be a valid HTML color (must starts with a "#").');
                return false;
              }
              if (this.getValue().length !== 4 && this.getValue().length !== 7) {
                alert('Background color must be a valid HTML color (example: #fff or #fffff)');
                return false;
              }
            }
          },
          {
            type: 'text',
            id: 'textcolor',
            label: 'Text color (HTML format) of the frame :',
            validate: function () {
              if (!this.getValue()) {
                alert('Text color cannot be empty.');
                return false;
              }
              if (this.getValue().substring(0, 1) != '#') {
                alert('Text color must be a valid HTML color (must starts with a "#").');
                return false;
              }
              if (this.getValue().length !== 4 && this.getValue().length !== 7) {
                alert('Text color must be a valid HTML color (example: #fff or #fffff)');
                return false;
              }
            }
          },
          {
            type: 'text',
            id: 'width',
            label: 'Width of the frame (expressed in px):',
            validate: function () {
              if (!this.getValue()) {
                alert('Width cannot be empty.');
                return false;
              }
              if (parseInt(this.getValue()) === 0) {
                alert('Width must be upper than 0.');
                return false;
              }
            }
          }
        ]
      }
    ],
    onOk: function () {
      var dialog = this, border_color = dialog.getValueOf('frame-div', 'color'),
        border_thickness = parseInt(dialog.getValueOf('frame-div', 'thickness')),
        border_type = dialog.getValueOf('frame-div', 'type'),
        frame_padding = parseInt(dialog.getValueOf('frame-div', 'padding')),
        frame_backcolor = dialog.getValueOf('frame-div', 'backcolor'),
        frame_textcolor = dialog.getValueOf('frame-div', 'textcolor'),
        frame_width = parseInt(dialog.getValueOf('frame-div', 'width'));

      editor.insertHtml('<div class="frameContainer" style="border: ' + border_thickness + 'px ' + border_type + ' ' + border_color + '; padding: ' + frame_padding + 'px; background-color: ' + frame_backcolor + '; color: ' + frame_textcolor + '; width: ' + frame_width + 'px"></div>');
    }
  }
});
