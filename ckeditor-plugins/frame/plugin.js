CKEDITOR.plugins.add('frame', {
  icons: 'icon',
  init: function(editor) {
    editor.addCommand('frame', new CKEDITOR.dialogCommand('frame'));

    editor.ui.addButton('frame', {
      label: 'Insert frame',
      command: 'frame',
      toolbar: 'insert,0',
      icon: this.path + 'icons/icon.png',
    });

    CKEDITOR.dialog.add('frame', this.path + 'dialogs/frame.js');

    // Add the frame widget's styles when CKEditor is loaded.
    if (typeof editor.config.contentsCss == 'object') {
      editor.config.contentsCss.push(CKEDITOR.getUrl(this.path + 'frame.css'));
    } else {
      editor.config.contentsCss = [editor.config.contentsCss, CKEDITOR.getUrl(this.path + 'frame.css')];
    }
  }
});
